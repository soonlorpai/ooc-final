package io.muic.ooc.converter;

public class Characteristic {
    private String prefix;
    private String connector;
    private String firstWord;
    private String capitalization;

    public Characteristic(String p, String c, String fw, String cz) {
        prefix = p;
        connector = c;
        firstWord = fw;
        capitalization = cz;
    }

    public Characteristic(String c, String cz) {
        prefix = "";
        connector = c;
        capitalization = cz;
        firstWord = capitalization;
    }

    public Characteristic(String c, String fw, String cz) {
        prefix = "";
        connector = c;
        firstWord = fw;
        capitalization = cz;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getConnector() {
        return connector;
    }

    public String getFirstWord() {
        return firstWord;
    }

    public String getCapitalization() {
        return capitalization;
    }
}
