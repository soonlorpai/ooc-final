package io.muic.ooc.converter;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ConvertFactory {

    private final Map<String, Characteristic> REFERENCES = new HashMap<String, Characteristic>(){{
        put("camelCase", new Characteristic("","lower","proper"));
        put("snake_case", new Characteristic("_", "lower"));
        put("lowercase", new Characteristic("", "lower"));
        put("UPPERCASE", new Characteristic("", "upper"));
        put("kebab-case", new Characteristic("-", "lower"));
        put("PascalCase", new Characteristic("", "proper"));
        put("MySQLFriendly", new Characteristic("_", "lower"));
        put("OracleFriendly", new Characteristic("_", "upper"));
        put("MySQLFriendlyWithPrefix", new Characteristic("tbl_", "_", "lower", "lower"));
        put("OracleFriendlyWithPrefix", new Characteristic("TBL_", "_", "upper", "upper"));
    }};

    private final List<String> TYPES = new ArrayList<String>(){{
        add("class");
        add("property");
    }};

    public String create(List<String> words, String type, String format) {
        Characteristic f = REFERENCES.get(format);

        StringBuilder formattedName = new StringBuilder();

        if (type.equals("class")) formattedName.append(f.getPrefix());

        formattedName.append(formatCaps(words.get(0), f.getFirstWord()));
        words.remove(0);

        while (words.size() > 0) {
            formattedName.append(f.getConnector());
            formattedName.append(formatCaps(words.get(0), f.getCapitalization()));
            words.remove(0);
        }

        return formattedName.toString();
    }

    private String formatCaps(String word, String capitalization) {
        switch (capitalization) {
            case ("proper"): {
                StringBuilder out = new StringBuilder();
                out.append(Character.toUpperCase(word.charAt(0)));
                out.append(word.substring(1).toLowerCase());
                return out.toString();
            }
            case ("lower"): return word.toLowerCase();
            case ("upper"): return word.toUpperCase();
            default: return null;
        }
    }

    public boolean checkType(String type) {
        if (TYPES.contains(type)) return true;
        return false;
    }

    public boolean checkFormat(String format) {
        if (REFERENCES.containsKey(format)) return true;
        return false;
    }
}
