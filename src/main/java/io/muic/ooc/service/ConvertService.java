package io.muic.ooc.service;

import io.muic.ooc.converter.ConvertFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("convertService")
public class ConvertService {

    @Autowired
    ConvertFactory convertFactory;

    public boolean screen(String type, String format) {
        if (convertFactory.checkFormat(format) && convertFactory.checkType(type)) return true;
        return false;
    }

    public String convertName(String name, String type, String format) {
        List<String> words = nameToWords(name);

        return convertFactory.create(words, type, format);
    }

    private List<String> nameToWords(String name) {
        List<String> words = new LinkedList<>();

        int startIndex = 0, i = 1;
        while (i < name.length()) {
            if (Character.isUpperCase(name.charAt(i))) {
                words.add(name.substring(startIndex, i));
                startIndex = i;
            }
            i++;
        }
        words.add(name.substring(startIndex));

        return words;
    }

}
