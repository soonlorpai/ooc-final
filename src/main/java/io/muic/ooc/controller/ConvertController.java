package io.muic.ooc.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import io.muic.ooc.service.ConvertService;

import javax.validation.Valid;

@RestController
public class ConvertController {

    @Autowired
    private ConvertService convertService;

    @PostMapping(value = "/convert")
    public ResponseEntity<String> convert(@Valid @RequestBody Request request) {

        if (!convertService.screen(request.getType(), request.getFormat()))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(convertService.convertName(request.getName(), request.getType(), request.getFormat()), HttpStatus.OK);
    }

}
