package io.muic.ooc;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.muic.ooc.controller.ConvertController;
import io.muic.ooc.controller.Request;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@WebMvcTest(controllers = ConvertController.class)
public class ConvertApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectWriter objectWriter;

    private ObjectMapper objectMapper;

    @Autowired
    ConvertController convertController;

    private List<RequestTest> requestTests = new ArrayList<>();

    @PostConstruct
    public void readFromCSV() throws IOException {
        FileReader fileReader = new FileReader("src/main/resources/test-cases.csv");
        BufferedReader csvReader = new BufferedReader(fileReader);

        try {
            String row;
            String[] data;

            csvReader.readLine();

            while ((row = csvReader.readLine()) != null) {
                data = row.split(", ");
                RequestTest requestTest = new RequestTest(data[0], data[1], data[2], data[3]);
                requestTests.add(requestTest);
            }
        } catch (IOException ex) { ex.printStackTrace(); }

    }

    @Test
    public void testOutput() throws Exception {

        objectMapper = new ObjectMapper();
        objectWriter = objectMapper.writer().withDefaultPrettyPrinter();

        ModelMapper mapper = new ModelMapper();

        for (RequestTest reqTest: requestTests){
            Request req = mapper.map(reqTest, Request.class);

            String requestJson = objectWriter.writeValueAsString(req);

            mockMvc.perform(post("/convert").contentType(APPLICATION_JSON_UTF8)
                    .content(requestJson))
                    .andExpect(content().string(reqTest.getOutput()));
        }
    }
}
