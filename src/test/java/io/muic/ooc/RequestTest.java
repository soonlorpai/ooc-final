package io.muic.ooc;

public class RequestTest {

    private String name;

    private String type;

    private String format;

    private String output;

    public RequestTest(String name, String type, String format, String output) {
        this.name = name;
        this.type = type;
        this.format = format;
        this.output = output;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getFormat() {
        return format;
    }

    public String getOutput() {
        return output;
    }
}
